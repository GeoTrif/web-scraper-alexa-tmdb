package com.scraper.webscraperalexatmdb.view;

import com.scraper.webscraperalexatmdb.model.TmdbData;
import com.scraper.webscraperalexatmdb.service.GoogleSheetService;
import com.scraper.webscraperalexatmdb.service.TMDBService;
import com.scraper.webscraperalexatmdb.util.VaadinComponentsUtil;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.scraper.webscraperalexatmdb.util.Constants.*;

@Route(layout = MainView.class)
@UIScope
public class TMDBScraperView extends VerticalLayout implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(TMDBScraperView.class.getName());

    @Autowired
    private TMDBService tmdbService;

    @Autowired
    private GoogleSheetService googleSheetService;

    public TMDBScraperView() {
        H4 title = new H4(TMDB_VIEW_TITLE);
        TextField movieTitle = VaadinComponentsUtil.createTextField(MOVIE_TITLE_TEXT_FIELD_LABEL, TMDB_SITE_NAME_TEXT_FIELD_PLACEHOLDER);
        Button searchTmdb = new Button(SEARCH_TMDB_BUTTON_TEXT, new Icon(VaadinIcon.SEARCH));
        searchTmdb.addClickListener(event -> searchTmdbForData(movieTitle.getValue()));
        Button googleSheetValues = new Button(GOOGLE_SHEET_VALUES_BUTTON_TEXT, new Icon(VaadinIcon.FOLDER));
        googleSheetValues.addClickListener(event -> showGoogleSheetsValues());

        add(title, movieTitle, searchTmdb, googleSheetValues);
    }

    private void searchTmdbForData(String movieTitle) {
        Dialog tmdbResults = VaadinComponentsUtil.createDialog(StringUtils.EMPTY, DIALOG_WIDTH, DIALOG_HEIGHT);
        TmdbData tmdbData = tmdbService.searchMovieOnTmdb(movieTitle);
        List<Object> googleSheetValues = new ArrayList<>();

        Label titleLabel = new Label(tmdbData.getTitle());
        Image moviePicture = new Image(tmdbData.getPicture(), IMAGE_ALT);
        Label userScoreLabel = new Label(USER_SCORE_LABEL_TEXT + tmdbData.getUserScore());
        Label releaseDateLabel = new Label(RELEASE_DATE_LABEL_TEXT + tmdbData.getReleaseDate());
        Label runtimeLabel = new Label(RUNTIME_LABEL_TEXT + tmdbData.getRuntime());
        Label genresLabel = new Label(GENRES_LABEL_TEXT + tmdbData.getGenres());
        VerticalLayout verticalLayout = VaadinComponentsUtil.createVerticalLayout(titleLabel, moviePicture, userScoreLabel, releaseDateLabel, runtimeLabel, genresLabel);
        tmdbResults.add(verticalLayout);

        googleSheetValues.add(tmdbData.getTitle());
        googleSheetValues.add(tmdbData.getUserScore());
        googleSheetValues.add(tmdbData.getReleaseDate());
        googleSheetValues.add(tmdbData.getRuntime());
        googleSheetValues.add(tmdbData.getGenres());

        try {
            googleSheetService.writeValuesInSpreadsheet(TMDB_SPREADSHEET_ID, TMDB_SHEET_NAME, googleSheetValues);
        } catch (IOException e) {
            LOGGER.severe(String.format(WRITE_VALUES_IN_GOOGLE_SHEET_ERROR_MESSAGE, TMDB_SPREADSHEET_ID, TMDB_SHEET_NAME));
        }

        tmdbResults.open();
    }

    private void showGoogleSheetsValues() {
        Dialog tmdbResults = VaadinComponentsUtil.createDialog(StringUtils.EMPTY, DIALOG_WIDTH, DIALOG_HEIGHT);
        Grid<TmdbData> tmdbDataGrid = new Grid<>(TmdbData.class);

        List<List<Object>> rowResults = new ArrayList<>();
        List<TmdbData> gridData = new ArrayList<>();

        try {
            rowResults = googleSheetService.readFromSpreadsheet(TMDB_SPREADSHEET_ID, TMDB_SHEET_RANGE);
        } catch (IOException e) {
            LOGGER.severe(String.format(READ_VALUES_FROM_GOOGLE_SHEET_ERROR_MESSAGE, TMDB_SPREADSHEET_ID, TMDB_SHEET_RANGE));
        }

        for (List<Object> cellValue : rowResults) {
            TmdbData tmdbInfo = new TmdbData();
            tmdbInfo.setTitle(cellValue.get(0).toString());
            tmdbInfo.setUserScore(cellValue.get(1).toString());
            tmdbInfo.setReleaseDate(cellValue.get(2).toString());
            tmdbInfo.setRuntime(cellValue.get(3).toString());
            tmdbInfo.setGenres(cellValue.get(4).toString());

            gridData.add(tmdbInfo);
        }

        tmdbDataGrid.setItems(gridData);
        tmdbDataGrid.setColumns(TITLE_COLUMN_NAME, USER_SCORE_COLUMN_NAME, RELEASE_DATE_COLUMN_NAME, RUNTIME_COLUMN_NAME, GENRES_COLUMN_NAME);
        tmdbDataGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        tmdbResults.add(tmdbDataGrid);
        tmdbResults.open();
    }
}
