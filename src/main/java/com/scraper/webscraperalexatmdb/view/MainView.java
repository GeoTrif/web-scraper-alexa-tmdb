package com.scraper.webscraperalexatmdb.view;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;

@Route("")
@Theme(value = Material.class, variant = Material.DARK)
public class MainView extends AppLayout {

    public MainView() {
        setDrawerOpened(false);

        final DrawerToggle drawerToggle = new DrawerToggle();
        H4 title = new H4("Web Scraper");
        title.getStyle().set("color", "rgb(102, 168, 255)");
        addToNavbar(drawerToggle, title);

        Tabs sideMenu = new Tabs();
        sideMenu.add(
                getLinkAsTab("Alexa Site Info", AlexaSiteInfoScraperView.class, VaadinIcon.SEARCH),
                getLinkAsTab("TMDB", TMDBScraperView.class, VaadinIcon.BARCODE)
        );

        sideMenu.setOrientation(Tabs.Orientation.VERTICAL);

        addToDrawer(sideMenu);
    }

    private Tab getLinkAsTab(String label, Class view) {
        Tab tab = new Tab();
        tab.add(new RouterLink(label, view));
        return tab;
    }

    private Tab getLinkAsTab(String label, Class view, VaadinIcon icon) {
        Tab tab = getLinkAsTab(label, view);
        tab.addComponentAsFirst(new Icon(icon));
        return tab;
    }

}
