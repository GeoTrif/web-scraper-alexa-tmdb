package com.scraper.webscraperalexatmdb.view;

import com.scraper.webscraperalexatmdb.model.AlexaSiteInfoData;
import com.scraper.webscraperalexatmdb.service.AlexaSiteInfoService;
import com.scraper.webscraperalexatmdb.service.GoogleSheetService;
import com.scraper.webscraperalexatmdb.util.VaadinComponentsUtil;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.scraper.webscraperalexatmdb.util.Constants.*;

@Route(layout = MainView.class)
@UIScope
public class AlexaSiteInfoScraperView extends VerticalLayout implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(AlexaSiteInfoScraperView.class.getName());

    @Autowired
    private AlexaSiteInfoService alexaSiteInfoService;

    @Autowired
    private GoogleSheetService googleSheetService;

    public AlexaSiteInfoScraperView() {
        H4 title = new H4(ALEXA_VIEW_TITLE);
        TextField siteName = VaadinComponentsUtil.createTextField(SITE_NAME_TEXT_FIELD_LABEL, ALEXA_SITE_NAME_TEXT_FIELD_PLACEHOLDER);
        Button searchAlexaSiteInfo = new Button(SEARCH_ALEXA_SITE_INFO_BUTTON_TEXT, new Icon(VaadinIcon.SEARCH));
        searchAlexaSiteInfo.addClickListener(event -> searchSimilarWebForData(siteName.getValue()));
        Button googleSheetValues = new Button(GOOGLE_SHEET_VALUES_BUTTON_TEXT, new Icon(VaadinIcon.FOLDER));
        googleSheetValues.addClickListener(event -> showGoogleSheetsValues());

        add(title, siteName, searchAlexaSiteInfo, googleSheetValues);
    }

    private void searchSimilarWebForData(String siteName) {
        Dialog alexaResults = VaadinComponentsUtil.createDialog(StringUtils.EMPTY, DIALOG_WIDTH, DIALOG_HEIGHT);
        AlexaSiteInfoData alexaSiteInfoData = alexaSiteInfoService.resolveAlexaSiteInfoInformation(siteName);
        List<Object> googleSheetValues = new ArrayList<>();

        Label websiteNameLabel = new Label(WEBSITE_NAME_LABEL_TEXT + alexaSiteInfoData.getWebsiteName());
        Label optimizationOpportunitiesLabel = new Label(OPTIMIZATION_OPPORTUNITIES_LABEL_TEXT + alexaSiteInfoData.getOptimizationOpportunities());
        Label searchTrafficLabel = new Label(SEARCH_TRAFFIC_LABEL_TEXT + alexaSiteInfoData.getSearchTraffic());
        Label globalSiteRankingLabel = new Label(GLOBAL_SITE_RANKING_LABEL_TEXT + alexaSiteInfoData.getGlobalSiteRanking());
        Label dailyTimeOnSiteLabel = new Label(DAILY_TIME_ON_SITE_LABEL_TEXT + alexaSiteInfoData.getDailyTimeOnSite());
        VerticalLayout alexaResultsLayout = VaadinComponentsUtil.createVerticalLayout(websiteNameLabel, optimizationOpportunitiesLabel, searchTrafficLabel, globalSiteRankingLabel, dailyTimeOnSiteLabel);
        alexaResults.add(alexaResultsLayout);

        googleSheetValues.add(alexaSiteInfoData.getWebsiteName());
        googleSheetValues.add(alexaSiteInfoData.getOptimizationOpportunities());
        googleSheetValues.add(alexaSiteInfoData.getSearchTraffic());
        googleSheetValues.add(alexaSiteInfoData.getGlobalSiteRanking());
        googleSheetValues.add(alexaSiteInfoData.getDailyTimeOnSite());

        try {
            googleSheetService.writeValuesInSpreadsheet(ALEXA_SPREADSHEET_ID, ALEXA_SHEET_NAME, googleSheetValues);
        } catch (IOException e) {
            LOGGER.severe(String.format(WRITE_VALUES_IN_GOOGLE_SHEET_ERROR_MESSAGE, ALEXA_SPREADSHEET_ID, ALEXA_SHEET_NAME));
        }

        alexaResults.open();
    }

    private void showGoogleSheetsValues() {
        Dialog googleSheetResults = VaadinComponentsUtil.createDialog(StringUtils.EMPTY, DIALOG_WIDTH, DIALOG_HEIGHT);
        Grid<AlexaSiteInfoData> alexaDataGrid = new Grid<>(AlexaSiteInfoData.class);

        List<List<Object>> rowResults = new ArrayList<>();
        List<AlexaSiteInfoData> gridData = new ArrayList<>();

        try {
            rowResults = googleSheetService.readFromSpreadsheet(ALEXA_SPREADSHEET_ID, ALEXA_SHEET_RANGE);
        } catch (IOException e) {
            LOGGER.severe(String.format(READ_VALUES_FROM_GOOGLE_SHEET_ERROR_MESSAGE, ALEXA_SPREADSHEET_ID, ALEXA_SHEET_RANGE));
        }

        for (List<Object> cellValue : rowResults) {
            AlexaSiteInfoData alexaInfo = new AlexaSiteInfoData();
            alexaInfo.setWebsiteName(cellValue.get(0).toString());
            alexaInfo.setOptimizationOpportunities(cellValue.get(1).toString());
            alexaInfo.setSearchTraffic(cellValue.get(2).toString());
            alexaInfo.setGlobalSiteRanking(cellValue.get(3).toString());
            alexaInfo.setDailyTimeOnSite(cellValue.get(4).toString());

            gridData.add(alexaInfo);
        }

        alexaDataGrid.setItems(gridData);
        alexaDataGrid.setColumns(WEBSITE_NAME_COLUMN_NAME, OPTIMIZATION_OPPORTUNITIES_COLUMN_NAME, SEARCH_TRAFFIC_COLUMN_NAME, GLOBAL_SITE_RANKING_COLUMN_NAME, DAILY_TIME_ON_SITE_COLUMN_NAME);
        alexaDataGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        googleSheetResults.add(alexaDataGrid);
        googleSheetResults.open();
    }
}
