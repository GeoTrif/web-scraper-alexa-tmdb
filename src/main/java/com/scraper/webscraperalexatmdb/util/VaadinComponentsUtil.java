package com.scraper.webscraperalexatmdb.util;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

public class VaadinComponentsUtil {

    public static TextField createTextField(String label, String placeholder) {
        TextField textField = new TextField();
        textField.setLabel(label);
        textField.setPlaceholder(placeholder);

        return textField;
    }

    public static Dialog createDialog(String label, String width, String height) {
        Dialog dialog = new Dialog();
        dialog.add(new Label(label));
        dialog.setWidth(width);
        dialog.setHeight(height);

        return dialog;
    }

    public static VerticalLayout createVerticalLayout(Component... components) {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(components);
        verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        return verticalLayout;
    }
}
