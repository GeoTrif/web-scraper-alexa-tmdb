package com.scraper.webscraperalexatmdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebScraperAlexaTmdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebScraperAlexaTmdbApplication.class, args);
	}

}
